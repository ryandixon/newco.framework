﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace NewCo.Data.Relational
{
    public class ServiceProviderInitializer : System.Data.Entity. DropCreateDatabaseIfModelChanges<NewCoRelationalContext>
    {
        protected override void Seed(NewCoRelationalContext context)
        {
            #region seed char32 alphabet table and procedures
            //foreach (var c in IntToChar32Encoder.Alphabet)
            for (var i = 0; i < IntToChar32Encoder.Alphabet.Length; i++)
                context.Char32EncodingAlphabet.Add(new Char32EncodingAlphabet { Character = IntToChar32Encoder.Alphabet[i].ToString(), Position = i });

            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Decode);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Encode);
            #endregion

            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.SeedIdentityFields);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.TRG_BusinessEntityAfterInsert);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.TRG_OrderAfterInsert);

            var originator = new BusinessEntity 
                { 
                    OrganizationName="Order Originator, Inc." ,
                    PrimaryContact = new Contact
                    {
                        Email = "rdixon+orderoriginatorinc@gmail.com",
                        FirstName = "Gotta",
                        LastName = "Shipit"
                    }

                };
            context.BusinessEntities.Add(originator);
            context.SaveChanges();

            var shipper = new BusinessEntity
                {
                    OrganizationName = "Duey, Cheatem, & Howe, Inc.",
                    PrimaryContact = new Contact
                    {
                        Email = "rdixon+legalshipinc@gmail.com",
                        FirstName = "Wegonna",
                        LastName = "Cheatem"
                    }                    
                };
            context.BusinessEntities.Add(shipper);
            context.SaveChanges();

            var serviceProvider = new ServiceProvider
            {
                Id = 50001,
                BusinessEntity= new BusinessEntity() {
                    OrganizationName = "Fast Driver, Inc.",
                    PrimaryContact = new Contact
                    {
                         Email = "rdixon+fastdriver@gmail.com",
                         FirstName = "Fast",
                         LastName = "Driver",                         
                    }
                },
                MobileNumber = "3035551111",
                ProviderNumber = 50001L.ToChar32String(),
                EnrollmentStatus = ServiceProviderEnrollmentStatus.Active
            };
            context.ServiceProviders.Add(serviceProvider);
            context.SaveChanges();

            var order = new Order
            {
                OriginatorId = originator.Id,
                ServiceProviderId = serviceProvider.Id,
                ServiceProviderOfferAmount = 55M,
            };
            order.SecondaryParties.Add(new OrderSecondaryParty
            {
                BusinessEntityId = shipper.Id,
                Relationship = SecondaryPartyRelationship.Shipper
            });
            context.Orders.Add(order);
            context.SaveChanges();
        }

        private void ExecuteNonQueryBatch(string connectionString, string sqlStatements)
        {
            if (sqlStatements == null) throw new ArgumentNullException("sqlStatements");
            if (sqlStatements == null) throw new ArgumentNullException("connectionString");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                Regex r = new Regex(@"^(\s|\t)*go(\s\t)?.*", RegexOptions.Multiline | RegexOptions.IgnoreCase);

                connection.Open();

                foreach (string s in r.Split(sqlStatements))
                {
                    //Skip empty statements, in case of a GO and trailing blanks or something
                    string thisStatement = s.Trim();
                    if (String.IsNullOrEmpty(thisStatement)) continue;

                    using (SqlCommand cmd = new SqlCommand(thisStatement, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

    }
}
