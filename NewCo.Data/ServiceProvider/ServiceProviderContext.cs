﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Infrastructure;
using NewCo.Data;

namespace NewCo.Data.ServiceProvider
{
    public class ServiceProviderContext: ContextBase
    {
        private readonly System.Security.Principal.IIdentity userIdentity;
        public ServiceProviderContext(System.Security.Principal.IIdentity userIdentity)
            : base("NewCoRelationalContext")
        {
            this.userIdentity = userIdentity;
            Construct();
        }
        public ServiceProviderContext()
            : base("NewCoRelationalContext")
        {
            Construct();
            
        }

        private void Construct()
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized +=
                (sender, e) => DateTimeKindAttribute.Apply(e.Entity);
        }

        public void RejectChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        {
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                    case EntityState.Deleted:
                        {
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                    case EntityState.Added:
                        {
                            entry.State = EntityState.Detached;
                            break;
                        }
                }
            }
        }
        
        public void SetEntryState(object entity, EntityState state)
        {
            this.Entry(entity).State = state;
        }
        public override int SaveChanges()
        {
            try
            {
                var utcNowAuditDate = DateTime.UtcNow;

                //Set audit info 
                var creates = from e in ChangeTracker.Entries<ICreateHistory>().AsQueryable()
                              where e.State == EntityState.Added
                              select e;
                if (creates != null)
                    foreach (DbEntityEntry<ICreateHistory> dbEntityEntry in creates)
                    {
                        dbEntityEntry.Entity.CreateTimeUTC = utcNowAuditDate;
                        dbEntityEntry.Entity.CreatedBy = userIdentity == null ? "unknown" : userIdentity.Name;
                    }

                var updates = from e in ChangeTracker.Entries<IUpdateHistory>().AsQueryable()
                              where e.State == EntityState.Modified
                              select e;
                if (updates != null)
                    foreach (DbEntityEntry<IUpdateHistory> dbEntityEntry in updates)
                    {
                        dbEntityEntry.Entity.LastUpdatedTimeUTC = utcNowAuditDate;
                        dbEntityEntry.Entity.LastUpdatedBy = userIdentity == null ? "unknown" : userIdentity.Name;
                    }

                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DbSet<Accessorial> Accessorials { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<BusinessEntity> BusinessEntities { get; set; }
        public DbSet<BusinessEntityExternalIdentifier> BusinessEntityExternalIdentifiers { get; set; }
        public DbSet<BusinessEntityGroup> BusinessEntityGroups { get; set; }
        public DbSet<BusinessEntityGroupMembership> BusinessEntityGroupMemberships { get; set; }
        public DbSet<BusinessEntityRelationship> BusinessEntityRelationships { get; set; }
        public DbSet<BusinessEntitySettlementMethod> BusinessEntitySettlementMethods { get; set; }
        public DbSet<Char32EncodingAlphabet> Char32EncodingAlphabet { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<DataSync> DataSyncs { get; set; }
        public DbSet<ExternalOrderId> ExternalOrderIds { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderAccessorial> OrderAccessorials { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        public DbSet<OrderSecondaryParty> OrderSecondaryParties { get; set; }
        public DbSet<PerformanceRating> PerformanceRatings { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceProviderSyncHistory> ServiceProviderSyncHistories { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopTransportPiece> StopTransportPieces { get; set; }
        public DbSet<TransportPiece> TransportPieces { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<Char32EncodingAlphabet>()
                .Property(p => p.Character)
                .HasColumnType("char");

            builder.Conventions.Remove<PluralizingTableNameConvention>();

            builder.Configurations.Add(new OrderTypeConfiguration());

            //builder.Entity<OrderSecondaryParty>().HasRequired(osp => osp.SecondaryParty).WillCascadeOnDelete(false);
            //builder.Entity<OrderSecondaryParty>().HasForeignKey(x )
            
        }
    }
}
