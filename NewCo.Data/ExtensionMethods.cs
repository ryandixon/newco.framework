﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public static class ExtensionMethods
    {
        ///// <summary>
        ///// Converts an int64 to a short character string
        ///// </summary>
        ///// <param name="i"></param>
        ///// <returns></returns>
        //public static string ToChar32String(this long i)
        //{
        //    return IntToChar32Encoder.Encode(i);
        //}

        ///// <summary>
        ///// Converts a string containing a 32char encoded value to a Int64 (long).
        ///// </summary>
        ///// <param name="s"></param>
        ///// <returns></returns>
        //public static long ToLong(this string s)
        //{
        //    return IntToChar32Encoder.Decode(s);
        //}
    
    }

    /// <summary>
    /// Uses 32 characters to generate a unique code from an integer Native Bijective Function: https://gist.github.com/dgritsko/9554733
    /// </summary>
    public class IDEncoder
    {
        //public static readonly char[] Alphabet = "ABCDEFGHIJKMNPQRSTUVWXYZ23456789".ToCharArray();

        /*
         * This is the letters A-Z EXCEPT L & O plus the numbers 2-9. L, 1, O, O where omitted to avoid visual confusion.
         * The "random" order of the letters and numbers obfuscates the sequence nature of the order id from the end user, making it
         */
        //public static readonly char[] Alphabet = "3T7DFAEHCGK6V2XMNIJS8B5UP4QRY9ZW".ToCharArray();

        internal static readonly Dictionary<char, char[]> Alphabets = new Dictionary<char, char[]>{
            {'A', new char [] {'Z','4','2','S','F','T','7','N','9','H','L','P','V','M','8','A','3','6','U','G','X','Y','5','B','Q','J','W','K','E','C','D','R'}},
            {'B', new char [] {'H','D','S','3','X','P','M','8','C','W','A','J','N','9','Z','Y','L','V','2','4','7','E','F','Q','5','G','U','K','6','T','R','B'}},
            {'C', new char [] {'2','N','E','9','D','K','3','R','H','A','Z','B','C','U','Q','M','4','J','5','G','F','T','V','6','8','Y','W','7','L','X','S','P'}},
            {'D', new char [] {'J','Y','5','F','T','E','L','8','N','P','U','3','X','A','C','7','D','4','Z','R','H','V','Q','B','9','S','W','M','K','2','G','6'}},
            {'E', new char [] {'4','9','S','M','A','C','Y','Q','R','2','H','V','K','U','5','P','T','N','3','7','G','6','W','F','B','D','J','E','Z','L','8','X'}},
            {'F', new char [] {'K','L','H','X','R','8','D','7','V','C','9','N','B','E','T','G','6','F','3','P','U','A','4','2','M','Z','Q','Y','5','J','S','W'}},
            {'G', new char [] {'5','T','8','3','6','Y','R','M','W','N','V','D','Z','Q','E','2','L','U','4','P','K','F','A','C','H','J','B','X','G','9','S','7'}},
            {'H', new char [] {'N','6','W','B','M','Y','C','8','4','3','P','7','K','E','2','J','X','L','5','A','U','S','F','R','Q','D','9','G','T','Z','V','H'}},
            {'L', new char [] {'6','F','M','L','3','T','R','Q','9','G','E','Y','C','W','V','A','J','8','5','P','4','Z','K','2','N','U','X','S','7','D','B','H'}},
            {'J', new char [] {'P','R','B','S','L','Q','8','7','D','W','6','T','3','E','J','Y','X','U','5','2','A','C','N','G','V','K','4','H','F','M','9','Z'}},
            {'K', new char [] {'8','Z','Y','W','X','L','M','P','F','9','U','G','R','V','7','E','B','D','6','H','C','N','2','S','3','5','Q','4','J','A','T','K'}},
            {'M', new char [] {'R','B','Q','7','F','G','4','9','M','K','P','C','H','L','Z','6','U','3','8','V','J','W','2','Y','X','S','E','N','T','5','A','D'}},
            {'N', new char [] {'9','K','E','D','W','B','M','S','T','2','F','5','6','Y','N','R','7','P','A','8','J','Z','C','L','4','H','Q','G','3','X','V','U'}},
            {'P', new char [] {'S','W','5','J','C','7','Y','9','T','B','4','R','Q','G','D','F','M','A','E','P','X','H','K','2','8','6','L','Z','N','V','3','U'}},
            {'Q', new char [] {'B','7','T','S','R','3','F','P','Z','N','W','K','H','4','5','2','X','V','A','Y','U','G','L','9','C','M','8','E','J','Q','D','6'}},
            {'R', new char [] {'U','G','L','3','9','X','V','8','2','Z','J','D','6','H','P','M','C','K','B','E','4','S','T','N','F','Q','5','7','W','A','Y','R'}},
            {'S', new char [] {'C','S','8','9','P','T','B','Q','6','F','D','7','W','2','H','A','V','4','G','R','5','Y','U','N','J','Z','X','M','3','K','E','L'}},
            {'T', new char [] {'V','3','W','F','6','N','R','9','B','S','2','Y','L','H','5','U','A','Q','D','7','C','E','4','8','K','P','T','J','G','Z','M','X'}},
            {'U', new char [] {'E','D','N','Q','M','J','9','T','G','6','W','U','A','4','X','L','R','F','H','P','K','S','B','Y','2','C','3','8','5','Z','7','V'}},
            {'V', new char [] {'W','N','B','V','3','F','P','9','J','L','K','M','Z','Q','H','7','5','2','D','X','R','T','E','4','U','6','C','S','8','G','A','Y'}},
            {'W', new char [] {'F','Y','2','4','H','A','6','Q','P','V','B','D','N','7','9','S','J','M','L','C','U','3','R','E','Z','T','G','K','W','X','8','5'}},
            {'X', new char [] {'Y','9','P','B','X','6','J','8','S','A','Z','4','D','M','V','F','W','C','L','Q','U','E','T','N','5','G','7','2','K','3','R','H'}},
            {'Y', new char [] {'H','J','E','K','D','4','2','S','Y','P','T','X','5','8','M','3','B','V','G','6','W','F','Z','U','7','Q','9','C','N','R','A','L'}},
            {'Z', new char [] {'Z','T','5','P','U','W','F','9','3','Y','G','K','M','N','B','J','S','E','L','D','7','V','6','A','8','Q','2','4','X','H','R','C'}},
            {'2', new char [] {'L','5','T','X','B','S','Y','Q','8','D','9','G','E','7','2','A','6','3','K','R','C','W','F','N','H','4','U','P','J','V','M','Z'}},
            {'3', new char [] {'3','E','L','6','R','N','C','9','B','T','Y','8','2','Q','P','V','J','S','H','7','D','5','G','X','F','W','M','A','U','K','Z','4'}},
            {'4', new char [] {'J','Q','8','D','7','L','U','S','F','4','P','2','T','9','E','H','Z','B','N','M','K','G','V','C','R','W','X','5','3','A','6','Y'}},
            {'5', new char [] {'4','Y','V','J','N','E','A','9','K','H','G','U','F','T','6','7','L','2','Q','Z','P','S','W','R','D','8','B','X','C','5','M','3'}},
            {'6', new char [] {'M','B','N','U','5','A','R','T','Q','X','8','L','6','C','W','S','Z','J','P','F','Y','3','4','7','H','K','D','E','V','2','9','G'}},
            {'7', new char [] {'6','K','B','Z','J','7','5','9','S','8','U','D','R','Q','H','E','A','4','P','V','X','3','Y','F','N','G','2','T','M','L','W','C'}},
            {'8', new char [] {'N','W','2','8','Z','Y','J','Q','V','H','K','6','F','B','9','3','P','S','M','7','X','D','A','R','U','4','T','E','5','C','G','L'}},
            {'9', new char [] {'7','6','P','F','G','W','2','9','Z','V','C','U','5','R','T','J','4','E','N','L','8','K','D','Y','S','M','Q','3','A','B','X','H'}},

        };
        //public static readonly int Base = Alphabet.Length;

        public static string EncodeUsingChars(Int64 i, char[] alphabet)
        {
            if (i == 0) return alphabet[0].ToString();

            var s = string.Empty;

            while (i > 0)
            {
                s += alphabet[i % alphabet.Length];
                i = i / alphabet.Length;
            }

            return string.Join(string.Empty, s.Reverse());
        }

        public static Int64 DecodeUsingChars(string s, char[] alphabet)
        {
            Int64 i = 0;

            if (IsTooLong(s))
                return 0;

            foreach (var c in s)
            {
                i = (i * alphabet.Length) + Array.FindIndex(alphabet, item => item == c);
            }

            return i;
        }

        internal static bool HasValidCheckCharacter(string input)
        {
            //based on: https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm
            int factor = 1;
            int sum = 0;
            int n = 32;

            char alphabetKey = input[0];
            // Starting from the right, work leftwards
            // Now, the initial "factor" will always be "1" 
            // since the last character is the check character
            for (int i = input.Length - 1; i >= 0; i--)
            {
                //int codePoint = CodePointFromCharacter(input[i]);
                int codePoint = Alphabets[alphabetKey].ToList().IndexOf(input[i]);
                int addend = factor * codePoint;

                // Alternate the "factor" that each "codePoint" is multiplied by
                factor = (factor == 2) ? 1 : 2;

                // Sum the digits of the "addend" as expressed in base "n"
                addend = (addend / n) + (addend % n);
                sum += addend;
            }

            int remainder = sum % n;

            return (remainder == 0);
        }

        /// <summary>
        /// If encoded value looks like it will result in something bigger than a long, throw an exception.
        /// </summary>
        /// <param name="encodedNumber"></param>
        /// <returns></returns>
        private static bool IsTooLong(string encodedId)
        {
            if (encodedId.Length > 13)
                throw new Exception("Encoded number is too long");

            return false;
        }

        /// <summary>
        /// Decodes a NewCo id into a long
        /// </summary>
        /// <param name="encodedId"></param>
        /// <returns></returns>
        public static long DecodeIdToLong(string encodedId)
        {
            if (encodedId.Length > 13)
                throw new Exception("Encoded value is too big");

            if (IsTooLong(encodedId) || encodedId.Length < 3)
                return 0;

            if (!HasValidCheckCharacter(encodedId))
                return 0;

            //get the first char, which tells us which alphabet to use
            var alphabetKey = encodedId.ToCharArray()[0];

            //trim the first 2 chars to isolate the actual long value
            //var encodedLong = encodedNumber.Substring(2);

            //Trim the Checksum char and the alphabetKey
            var encodedLong = String.Join("",encodedId.ToCharArray(1, encodedId.Length - 2)); // encodedNumber.Substring(0, encodedNumber.Length - 1);

            //decode using the correct alphabet
            return DecodeUsingChars(encodedLong, Alphabets[alphabetKey]);
        }

        public static string GenerateRandomizedIdHash(long id)
        {
            //var alphabetKey = "ABCDEFGHIJKMNPQRSTUVWXYZ23456789"[StrongRandom.Next(0, 31)];
            var alphabetKey = Alphabets.Keys.ElementAt(StrongRandom.Next(0, Alphabets.Keys.Count - 1));

            var rtn = alphabetKey + EncodeUsingChars(id, Alphabets[alphabetKey]);

            return rtn + GenerateCheckCharacter(rtn, Alphabets[alphabetKey]).ToString();
        }

        static char GenerateCheckCharacter(string input, char[] alphabet)
        {

            int factor = 2;
            int sum = 0;
            int n = 32;

            // Starting from the right and working leftwards is easier since 
            // the initial "factor" will always be "2" 
            for (int i = input.Length - 1; i >= 0; i--)
            {
                int codePoint = CodePointFromCharacter(input[i], alphabet);
                int addend = factor * codePoint;

                // Alternate the "factor" that each "codePoint" is multiplied by
                factor = (factor == 2) ? 1 : 2;

                // Sum the digits of the "addend" as expressed in base "n"
                addend = (addend / n) + (addend % n);
                sum += addend;
            }

            // Calculate the number that must be added to the "sum" 
            // to make it divisible by "n"
            int remainder = sum % n;
            int checkCodePoint = (n - remainder) % n;

            return alphabet[checkCodePoint];
        }

        static int CodePointFromCharacter(char character, char[] alphabet)
        {
            for (var i = 0; i < alphabet.Length; i++)
                if (alphabet[i] == character)
                    return i;

            return -1;
        }

        static bool ValidateCheckCharacter(string input, char[] alphabet)
        {

            int factor = 1;
            int sum = 0;
            int n = 32;

            // Starting from the right, work leftwards
            // Now, the initial "factor" will always be "1" 
            // since the last character is the check character
            for (int i = input.Length - 1; i >= 0; i--)
            {
                int codePoint = CodePointFromCharacter(input[i], alphabet);
                int addend = factor * codePoint;

                // Alternate the "factor" that each "codePoint" is multiplied by
                factor = (factor == 2) ? 1 : 2;

                // Sum the digits of the "addend" as expressed in base "n"
                addend = (addend / n) + (addend % n);
                sum += addend;
            }

            int remainder = sum % n;

            return (remainder == 0);
        }

    }

    public static class StrongRandom
    {
        [ThreadStatic]
        private static Random _random;

        public static int Next(int inclusiveLowerBound, int inclusiveUpperBound)
        {
            if (_random == null)
            {
                var cryptoResult = new byte[4];
                new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(cryptoResult);

                int seed = BitConverter.ToInt32(cryptoResult, 0);

                _random = new Random(seed);
            }

            // upper bound of Random.Next is exclusive
            int exclusiveUpperBound = inclusiveUpperBound + 1;
            return _random.Next(inclusiveLowerBound, exclusiveUpperBound);
        }
    }
}
