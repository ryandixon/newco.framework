﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public abstract class ContextBase : DbContext, IContextBase
    {
        private static NLog.Logger logger = null;
        protected readonly System.Security.Principal.IIdentity userIdentity;

        public ContextBase(string connectionString, System.Security.Principal.IIdentity userIdentity)
            : base(connectionString)
        {
            this.userIdentity = userIdentity;
            Construct();
        }
        public ContextBase(string connectionString)
            : base(connectionString)
        {
            Construct();
        }

        protected virtual void Construct()
        {
            //((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += (sender, e) => DateTimeKindAttribute.Apply(e.Entity);
            logger = NLog.LogManager.GetCurrentClassLogger();

            //there is a performance hit for even setting the database.log delegate, so don't do so unless necessary.
            if (NLog.LogManager.IsLoggingEnabled())
               this.Database.Log = delegate(string message) { logger.Trace(message); };
        }

        public virtual void RejectChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        {
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                    case EntityState.Deleted:
                        {
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                    case EntityState.Added:
                        {
                            entry.State = EntityState.Detached;
                            break;
                        }
                }
            }
        }

        protected string GetProviderName(string connectionString)
        {
            //foreach(var cxnString in ConfigurationManager.ConnectionStrings)
            for (var i=0; i< ConfigurationManager.ConnectionStrings.Count; i++)
            {
                if (ConfigurationManager.ConnectionStrings[i].ConnectionString == connectionString)
                    return ConfigurationManager.ConnectionStrings[i].ProviderName;
            }

            return null;
        }
        public virtual void SetEntryState(object entity, EntityState state)
        {
            this.Entry(entity).State = state;
        }
        public override int SaveChanges()
        {
            try
            {
                var utcNowAuditDate = DateTime.UtcNow;

                //Set audit info 
                var creates = from e in ChangeTracker.Entries<ICreateHistory>().AsQueryable()
                              where e.State == EntityState.Added
                              select e;
                if (creates != null)
                    foreach (DbEntityEntry<ICreateHistory> dbEntityEntry in creates)
                    {
                        dbEntityEntry.Entity.CreateDateUTC = utcNowAuditDate;
                        dbEntityEntry.Entity.CreatedBy = userIdentity == null ? "unknown" : userIdentity.Name;
                    }

                var updates = from e in ChangeTracker.Entries<IUpdateHistory>().AsQueryable()
                              where e.State == EntityState.Modified
                              select e;
                if (updates != null)
                    foreach (DbEntityEntry<IUpdateHistory> dbEntityEntry in updates)
                    {
                        dbEntityEntry.Entity.LastUpdatedDateUTC = utcNowAuditDate;
                        dbEntityEntry.Entity.LastUpdatedBy = userIdentity == null ? "unknown" : userIdentity.Name;
                    }

                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
