﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewCo.Data
{
    public abstract class Char32EncodingAlphabetBase
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string AlphabetKey { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(1)]
        public string Character { get; set; }

        public int Position { get; set; }
    }
}
