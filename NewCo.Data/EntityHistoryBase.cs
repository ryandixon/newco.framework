﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public abstract class EntityHistoryBase: ICreateHistory, IUpdateHistory
    {
        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateDateUTC { get; set; }
        [StringLength(48)]
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedDateUTC { get; set; }
        [StringLength(48)]
        public string LastUpdatedBy { get; set; }
    }
}
