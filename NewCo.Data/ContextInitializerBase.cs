﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewCo.Data
{
    //public class DropCreateDatabaseIfModelChangesInitializerBase<T> where T : System.Data.Entity.DropCreateDatabaseIfModelChanges<System.Data.Entity.DbContext>
    public class DropCreateDatabaseIfModelChangesInitializerBase<TContext> : System.Data.Entity.DropCreateDatabaseIfModelChanges<TContext> 
        where TContext : System.Data.Entity.DbContext
    {

        protected void ExecuteNonQueryBatch(string connectionString, string sqlStatements)
        {
            if (sqlStatements == null) throw new ArgumentNullException("sqlStatements");
            if (sqlStatements == null) throw new ArgumentNullException("connectionString");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                Regex r = new Regex(@"^(\s|\t)*go(\s\t)?.*", RegexOptions.Multiline | RegexOptions.IgnoreCase);

                connection.Open();

                foreach (string s in r.Split(sqlStatements))
                {
                    //Skip empty statements, in case of a GO and trailing blanks or something
                    string thisStatement = s.Trim();
                    if (String.IsNullOrEmpty(thisStatement)) continue;

                    using (SqlCommand cmd = new SqlCommand(thisStatement, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

    }
}
