﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class OrderShard: ShardBase
    {
        [Index("idx_OrderShardingKey", IsUnique = true)]
        public string ShardingKey { get; set; }

    }
}
