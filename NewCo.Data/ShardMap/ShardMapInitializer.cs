﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class ShardMapInitializer: DropCreateDatabaseIfModelChangesInitializerBase<ShardMapContext>
    {
        protected override void Seed(ShardMapContext context)
        {
            var shard = new BusinessEntityShard
            {
                Status = ShardStatus.InActive,
                StatusDateUTC = DateTime.UtcNow,
                ConnectionStringValues = new ConnectionStringValue[] {
                    new ConnectionStringValue(@"Data Source=.\SQLExpress"),
                    new ConnectionStringValue(@"Initial Catalog=BE_"),
                    new ConnectionStringValue(@"User Id=NewCo"),
                    new ConnectionStringValue(@"Password=M@k3ItHapp3n")
                }
            };
        }

        

    }
}
