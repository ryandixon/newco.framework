﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class ShardMapContext: ContextBase
    {
        public ShardMapContext(System.Security.Principal.IIdentity userIdentity)
            : base("ShardMapContext", userIdentity)
        {

        }

        public DbSet<ConnectionStringValue> ConnectionStringValues { get; set; }
        public DbSet<OrderShard> OrderShards { get; set; }
        public DbSet<BusinessEntityShard> BusinessEntityShards { get; set; }
        public DbSet<ProductShard> ProductShards { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Conventions.Remove<PluralizingTableNameConvention>();

        }

    }
}
