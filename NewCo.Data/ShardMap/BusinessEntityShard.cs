﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class BusinessEntityShard: ShardBase
    {
        [Index("idx_BEShardingKey", IsUnique = true)]
        public string ShardingKey { get; set; }

    }
}
