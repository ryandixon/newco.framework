﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public abstract class ShardBase: EntityHistoryBase
    {
        [Key]
        public int Id { get; set; }
        
        //A char32 Enocding of the ID
        public abstract string ShardingKey { get; set; }

        [NotMapped]
        public ShardStatus Status { get; set; }

        [Required]
        [Column("Shard")]
        [StringLength(24)]
        public string StatusString { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime StatusDateUTC { get; set; }

        public virtual IEnumerable<ConnectionStringValue> ConnectionStringValues { get; set; }

        public string GetConnectionString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var cxnVal in this.ConnectionStringValues)
                sb.AppendFormat("{0}={1};", cxnVal.ValueName, cxnVal.Value);

            return sb.ToString();
        }

    }
    public enum ShardStatus { InActive = 1, ActiveEmpty, Building, Active }


}
