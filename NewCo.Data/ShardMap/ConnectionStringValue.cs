﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class ConnectionStringValue
    {
        public ConnectionStringValue()
        {
        }
        public ConnectionStringValue(string nameAndValue)
        {
            if (string.IsNullOrEmpty(nameAndValue))
                throw new Exception("parameter 'nameAndValue' cannot be empty");

            if (nameAndValue.IndexOf('=') < 0)
                throw new Exception("parameter 'nameAndValue' must be in the form of '<name>=<value>'");

            this.ValueName = nameAndValue.Split('=')[0].Trim();
            this.Value = nameAndValue.Split('=')[1].Trim();
        }
        public int Id { get; set; }
        public int ShardMapId { get; set; }

        public string ValueName { get; set; }
        public string Value { get; set; }
    }
}
