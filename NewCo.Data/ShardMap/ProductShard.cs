﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.ShardMap
{
    public class ProductShard: ShardBase
    {
        [Index("idx_ProductShardingKey", IsUnique = true)]
        public string ShardingKey { get; set; }

    }
}
