﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public abstract class ReplicateBase: EntityHistoryBase
    {
        public ReplicateBase()
        {
            SourceDBInstanceId = -1; //By Default, the current DB is the source
        }

        public abstract long Id { get; set; }

        /// <summary>
        /// The ID within the Shard Map of the database instance that holds the writeable order information. A value of -1 indicates that the current instance is the primary/source record. Any value > 0, indicates that the source information resides in a remote DB.
        /// </summary>
        public int SourceDBInstanceId { get; set; }

        /// <summary>
        /// The Id value of the order record within the source database.
        /// </summary>
        public long? SourceDBOrderId { get; set; }

        /// <summary>
        /// Stores the date in UTC of the last time the record was replicated from (when this is the source entity) or to (when this is the remote instance) remote instances
        /// </summary>
        public DateTime LastReplicationDateUTC { get; set; }

        [NotMapped]
        public bool IsSourceCopy
        {
            get { return SourceDBInstanceId == -1; }
        }

    }
}
