﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GenerateCheckChar')
	DROP FUNCTION dbo.GenerateCheckChar;
GO
--here's a comment
CREATE FUNCTION dbo.GenerateCheckChar(@stringVal varchar(256), @alphabetKey char)
RETURNS char
AS 
BEGIN
	--BASED on exmaple from: https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm

	DECLARE @returnVal as char, @iter as int, @factor as int, @alphabetLength as int, @Sum as int;
	SET @factor=2;
	SET @alphabetLength= 32;

	SET @iter = LEN(@stringVal);
	WHILE @iter > 0
	BEGIN
		DECLARE @codePoint as int, @c as char, @addend as int;

		SET @c = SUBSTRING(@stringVal, @iter, 1);

		SELECT @codePoint = Position FROM Char32EncodingAlphabet with (NOLOCK) WHERE AlphabetKey = @alphabetKey AND [Character] = @c
		SET @Addend = @factor * @codePoint

		SET @addend = (@addend / @alphabetLength) + (@addend % @alphabetLength);
		SET @SUM = @sum + @addend

		--Alternate the factor
		SET @factor = case WHEN @factor = 2 then 1 else 2 end;

		SET @iter = @iter - 1
	END

	SELECT @returnVal = [character] 
	FROM Char32EncodingAlphabet with (NOLOCK) 
	WHERE AlphabetKey = @alphabetKey 
		AND position = (@alphabetLength - (@sum % @alphabetLength)) % @alphabetLength

	return @returnVal
END;
GO