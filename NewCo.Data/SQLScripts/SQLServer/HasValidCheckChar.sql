﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'HasValidCheckChar')
	DROP FUNCTION dbo.HasValidCheckChar;
GO
--here's a comment
CREATE FUNCTION dbo.HasValidCheckChar(@stringVal varchar(256), @alphabetKey char)
RETURNS bit
AS 
BEGIN
	--BASED on exmaple from: https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm

	DECLARE @returnVal as char, @iter as int, @factor as int, @alphabetLength as int, @Sum as int;
	SET @factor=1;
	SET @alphabetLength= 32;

	SET @iter = LEN(@stringVal) - 1;
	WHILE @iter > 0
	BEGIN
		DECLARE @codePoint as int, @c as char, @addend as int;

		SET @c = SUBSTRING(@stringVal, @iter, 1);

		SELECT @addend = @factor * position  FROM Char32EncodingAlphabet with (nolock) where AlphabetKey = @alphabetKey and [Character] = @c

		--Sum the digits of the "addend" as expressed in base 32
		SET @addend = (@addend / @alphabetLength) + (@addend % @alphabetLength)
		SET @Sum = @sum + @addend

		--Alternate the factor
		SET @factor = case WHEN @factor = 2 then 1 else 2 end;

		SET @iter = @iter - 1
	END

	return case when @sum % @alphabetLength = 0 then 1 else 0 end;
END;
GO