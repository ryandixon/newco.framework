﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GenerateRandomizedIdHash')
	DROP FUNCTION GenerateRandomizedIdHash;
GO

CREATE FUNCTION dbo.GenerateRandomizedIdHash(@id Bigint)
RETURNS varchar(16)
AS 
BEGIN
	DECLARE @alphabetKey as varchar(1), @returnVal as varchar(16);

	/*
	Format: <randomly chosen alphabet key char><long encoded using the randomly chosen alphabet><a checkcharacter>
	*/

	select @alphabetKey = SUBSTRING('ABCDEFGHIJKMNPQRSTUVWXYZ23456789',ABS(CHECKSUM(NewId())) % 32, 1)
	--Put the alphabet key on the front of the encoded value
	SET @returnVal = @alphabetKey + dbo.Char32Encode(@id, Cast(@alphabetKey as char)) 
		
	--Return the value with the check char tacked on the end
	return @returnVal + dbo.GenerateCheckChar(@returnVal, @alphabetKey)

END;
GO

