﻿IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spGetNextEncodedSequenceValue')
DROP PROCEDURE spGetNextEncodedSequenceValue
GO

CREATE PROCEDURE spGetNextEncodedSequenceValue 
@BusinessEntityId INT, 
@SequenceType INT,
@newValue varchar(32) OUT
AS
	--Can I force a transaction here that never waits on a Parent transaction (or is that the default behaviour)???
	BEGIN TRANSACTION 

	DECLARE @newInt BigInt
	DECLARE @ID table (ID int)

	UPDATE [Sequence] WITH (ROWLOCK)
	Set LastValue = LastValue + 1
	OUTPUT inserted.LastValue
		INTO @Id
	WHERE 
		BusinessEntityId = @BusinessEntityId
		And [Type] = @SequenceType

	COMMIT TRANSACTION

	SELECT TOP 1 @newValue = dbo.Char32Encode(id) FROM @ID
GO