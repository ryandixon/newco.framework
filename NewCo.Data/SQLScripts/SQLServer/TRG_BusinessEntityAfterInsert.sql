﻿/* After Insert trigger on employee table */
IF OBJECT_ID ('TRG_BusinessEntityAfterInsert','TR') IS NOT NULL
    DROP TRIGGER TRG_BusinessEntityAfterInsert;
GO

CREATE TRIGGER TRG_BusinessEntityAfterInsert 
ON dbo.BusinessEntity
AFTER INSERT AS
BEGIN
	DECLARE @id as bigint, @alphabetKey as varchar(1), @BENumber as varchar(16);

	SELECT @id = id from inserted;

	select @alphabetKey = SUBSTRING('ABCDEFGHIJKMNPQRSTUVWXYZ23456789',ABS(CHECKSUM(NewId())) % 32, 1)
	SET @BEnumber = @alphabetKey + dbo.Char32Encode(@id, Cast(@alphabetKey as char)) 
	SET @BENumber = @BENumber + dbo.GenerateCheckChar(@BENumber, @alphabetKey)
	
	UPDATE BusinessEntity
	SET BusinessEntityNumber = @BENumber
	WHERE
		Id = @id

END
GO