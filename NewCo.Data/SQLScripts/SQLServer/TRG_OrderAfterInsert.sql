﻿/* After Insert trigger on employee table */
IF OBJECT_ID ('TRG_OrderAfterInsert','TR') IS NOT NULL
    DROP TRIGGER TRG_OrderAfterInsert;
GO

CREATE TRIGGER TRG_OrderAfterInsert 
ON dbo.[Order]
AFTER INSERT AS
BEGIN
	DECLARE @id as bigint, @alphabetKey as varchar(1), @OrderNumber varchar(16);

	SELECT @id = id from inserted;

	select @alphabetKey = SUBSTRING('ABCDEFGHIJKMNPQRSTUVWXYZ23456789',ABS(CHECKSUM(NewId())) % 32, 1)
	SET @OrderNumber = @alphabetKey + dbo.Char32Encode(@id, Cast(@alphabetKey as char)) 
	SET @OrderNumber = @OrderNumber + dbo.GenerateCheckChar(@OrderNumber, @alphabetKey)


	UPDATE [Order]
	SET OrderNumber = @OrderNumber
	WHERE
		Id = @id

END
GO