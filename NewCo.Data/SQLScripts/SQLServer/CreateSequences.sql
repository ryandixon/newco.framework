﻿CREATE SEQUENCE seqBusinessEntityID
	AS BigInt
	START WITH 32768
	INCREMENT BY 1;
GO;

CREATE SEQUENCE seqServiceProviderId
	AS BigInt
	START WITH 32768
	INCREMENT BY 1;
GO;


CREATE SEQUENCE seqOrderId
	AS BigInt
	START WITH 32768
	INCREMENT BY 1;
GO;