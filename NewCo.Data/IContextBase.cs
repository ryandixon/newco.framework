﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public interface IContextBase
    {
        void RejectChanges();
        void SetEntryState(object entity, EntityState state);
    }
}
