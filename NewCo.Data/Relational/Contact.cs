﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Contact : EntityHistoryBase
    {
        public Contact ()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        [StringLength(40)]
        public string FirstName { get; set; }
        [StringLength(40)]
        public string MiddleName { get; set; }
        [StringLength(40)]
        public string LastName { get; set; }
        [StringLength(24)]
        public string PhoneNumber { get; set; }
        [StringLength(95)]
        public string Email { get; set; }
        //public Guid? AddressId { get; set; }

    }
}
