﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class ExternalOrderId : EntityHistoryBase
    {
        public ExternalOrderId()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        [Required]
        public Guid OrderId { get; set; }
        [Required]
        public ExternalOrderIdSource Source { get; set; }

        /// <summary>
        /// The business entity id of the source of the external Id (optional)
        /// </summary>
        public Int64? BusinessEntityId { get; set; }

        /// <summary>
        /// A descriptor of the External Id. Generally provided by the externally entity - e.g. Case Number, COPS Order Id, Agent's Customer's Account Id
        /// </summary>
        [Required]
        [StringLength(32)]
        public string IdType { get; set; }

        /// <summary>
        /// The value of the external ID
        /// </summary>
        [Required]
        [StringLength(32)]
        public string ExternalId { get; set; }

    }

    public enum ExternalOrderIdSource { Originator=1, SecondaryParty=2 }
}
