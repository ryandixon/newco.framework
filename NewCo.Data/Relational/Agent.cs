﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Agent : EntityHistoryBase
    {
        public Int64 Id { get; set; }
        [StringLength(16)]
        public string AgentNumber { get; set; }
        [Required]
        public Int64 BusinessEntityId { get; set; }

    }
}
