﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{

    public class OrderEvent
    {
        [Key]
        public long Id { get; set; }

        [Index("idx_OrderEventOrderId")]
        public long OrderId { get; set; }

        public Guid? StopId { get; set; }

        public string ServiceProviderCode { get; set; }

        public string Note { get; set; }

        public string SignorName { get; set; }

        public Guid? ImageId { get; set; }

        public DateTime EventTimeUTC { get; set; }        

    }

    public enum OrderEventType {Offer=1, Bid, Decline, Assign, StopArrive, StopDepart, StopDelay, Exception}
    /// <summary>
    /// The status of the order event. When the event originates on the server, this indicates that the status of the event on the remote device (the mobile app). When the event originates on the 
    ///     mobile app, they should arrive in the DB with a status of "received" and once processing has been completed (if any), be set to "read"
    /// </summary>
    public enum EventStatus { Sent=1, Received, Read}   
}
