﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class BusinessEntitySettlementMethod : EntityHistoryBase
    {
        public BusinessEntitySettlementMethod()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        public int BusinessEntityId { get; set; }

        public PaymentMethod PreferredPaymentMethod { get; set; }
        public CollectionMethod PreferredCollectionMethod { get; set; }

        /// <summary>
        /// Stores the id of this BE used by the service that processes the settlements. For Example: the PayeeID of this BE at our bank that we might use to send ACH deposits to this BE
        /// </summary>
        [StringLength(32)]
        public string BusinessEntitySettlementProcessorID { get; set; }

        /// <summary>
        /// Bank account number or Credit Card Number
        /// </summary>
        [StringLength(255)]
        public string EncyrptedAccountNumber { get; set; }
        [StringLength(12)]
        public string ABARoutingNumber { get; set; }
        [StringLength(255)]
        public string BankName { get; set; }
        [StringLength(32)]
        public string BankAccountType { get; set; }
        public bool BankInfoVerified { get; set; }

        [StringLength(32)]
        public string CCNickname { get; set; }
        [StringLength(4)]
        public string CCLast4Digits { get; set; }

        public DateTime CCExpiration { get; set; }
        public CreditCardType CCType { get; set; }
        [StringLength(255)]
        public string CCToken { get; set; }
        public bool CCInfoVerified { get; set; }
    }

    public enum PaymentMethod { Check, ACHTransfer, CreditDebitCard, SCI, CMS }
    public enum CollectionMethod { Invoice, ACHTransfer, CreditDebitCard }
    public enum BankAccountType { Checking, Savings}
    public enum CreditCardType { AmericanExpress, MastCard, Visa }
    
}
