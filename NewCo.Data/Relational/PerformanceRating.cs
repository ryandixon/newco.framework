﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class PerformanceRating : EntityHistoryBase
    {
        public PerformanceRating()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        /// <summary>
        /// BusinessEntityId of the entity that provided the rating
        /// </summary>
        [Required]
        public Int64 Rater { get; set; }

        /// <summary>
        /// Business Entity Id of the entity being rated
        /// </summary>
        [Required]
        public Int64 Ratee { get; set; }

        /// <summary>
        /// The order that is the basis for the rating, if any.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Rating. I.e. number of stars
        /// </summary>
        [Required]        
        public int Rating { get; set; }

        [StringLength(8000)]
        public string Comment { get; set; }

    }
}
