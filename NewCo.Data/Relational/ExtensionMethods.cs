﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public static class ExtensionMethods
    {
        public static DateTime LastChangeUTC<T>(this T entity) where T : ICreateHistory, IUpdateHistory
        {
            return entity.LastUpdatedDateUTC ?? entity.CreateDateUTC;
        }

        public static DateTime LargerValue(this DateTime currentVal, DateTime compareValue)
        {
            return currentVal > compareValue ? currentVal : compareValue;
        }


    }
}
