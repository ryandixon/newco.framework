﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class OrderAccessorial
    {
        [Key]
        [Column(Order=1)]
        public Guid OrderId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int AccessorialId { get; set; }

        public bool IsOptional { get; set; }
    }
}
