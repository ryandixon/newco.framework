﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace NewCo.Data.Relational
{
    public class NewCoRelationalInitializer : DropCreateDatabaseIfModelChangesInitializerBase<NewCoRelationalContext>
    {
        protected override void Seed(NewCoRelationalContext context)
        {
            #region seed char32 alphabet table and procedures
            foreach (var alphabet in IDEncoder.Alphabets)
                for (var i = 0; i < alphabet.Value.Length; i++ )
                    context.Char32EncodingAlphabet.Add(new Char32EncodingAlphabet { AlphabetKey = alphabet.Key.ToString(), Character = alphabet.Value[i].ToString(), Position = i });

            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Decode);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Encode);
            #endregion

            //ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.SeedIdentityFields);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.CreateSequences);
            //ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.TRG_BusinessEntityAfterInsert);
            //ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.TRG_OrderAfterInsert);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.HasValidCheckChar);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.GenerateCheckChar);

            var originator = new BusinessEntity 
                { 
                    OrganizationName="Order Originator, Inc." ,
                    PrimaryContact = new Contact
                    {
                        Email = "rdixon+orderoriginatorinc@gmail.com",
                        FirstName = "Gotta",
                        LastName = "Shipit"
                    }

                };
            context.BusinessEntities.Add(originator);
            context.SaveChanges();

            var shipper = new BusinessEntity
                {
                    OrganizationName = "Duey, Cheatem, & Howe, Inc.",
                    PrimaryContact = new Contact
                    {
                        Email = "rdixon+legalshipinc@gmail.com",
                        FirstName = "Wegonna",
                        LastName = "Cheatem"
                    }                    
                };
            context.BusinessEntities.Add(shipper);
            context.SaveChanges();

            var serviceProvider = new ServiceProvider
            {
                //Id = 50001,
                BusinessEntity= new BusinessEntity() {
                    OrganizationName = "Fast Driver, Inc.",
                    PrimaryContact = new Contact
                    {
                         Email = "rdixon+fastdriver@gmail.com",
                         FirstName = "Fast",
                         LastName = "Driver",                         
                    }
                },
                MobileNumber = "3035551111",
                //ProviderNumber = 50001L.ToChar32String(),
                EnrollmentStatus = ServiceProviderEnrollmentStatus.Active
            };
            context.ServiceProviders.Add(serviceProvider);
            context.SaveChanges();

            var order = new Order
            {
                OriginatorId = originator.Id,
                ServiceProviderId = serviceProvider.Id,
                ServiceProviderOfferAmount = 55M,
            };
            order.SecondaryParties.Add(new OrderSecondaryParty
            {
                BusinessEntityId = shipper.Id,
                Relationship = SecondaryPartyRelationship.Shipper
            });
            context.Orders.Add(order);
            context.SaveChanges();
        }


    }
}
