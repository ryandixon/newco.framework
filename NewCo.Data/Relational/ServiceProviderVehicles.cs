﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class ServiceProviderVehicles
    {
        public ServiceProviderVehicles()
        {
            this.Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }
        
        public long BusinessEntityId { get; set; }

        [NotMapped]
        public VehicleType Type { get; set; }
        [Column("Type")]
        public string TypeString
        {
            get { return this.Type.ToString(); }
            set { this.Type = (VehicleType)Enum.Parse(typeof(VehicleType), value); }
        }

        public bool IsRefridgerated { get; set; }
        public int MaxItemLengthInches { get; set; }
        public int MaxItemWeightPounds { get; set; }

        public bool HasLiftGate { get; set; }
        public bool HasHandTruck { get; set;  }
        public bool HasFunitureDolly { get; set; }

        public string ImageReferenceId { get; set; }
    }
    public enum VehicleType { Car, MiniVan, FullSizeVan, PickupTruck, EnclosedTruck}
}
