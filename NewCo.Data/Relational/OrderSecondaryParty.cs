﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class OrderSecondaryParty
    {
        [Key]
        [Column(Order=1)]
        public Guid OrderId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("SecondaryParty")]
        public Int64 BusinessEntityId { get; set; }
        
        
        public virtual BusinessEntity SecondaryParty { get; set; }

        [Required]
        public SecondaryPartyRelationship Relationship {get; set;}
    }

    public enum SecondaryPartyRelationship 
    {
        Shipper=1, 
        Receiver, 
        Agent,
        Other //A party probably with ReadOnly needs to view the data. Gov't monitor prehaps?
    }

    public class OrderSecondaryPartyTypeConfiguration : EntityTypeConfiguration<OrderSecondaryParty>
    {
        public OrderSecondaryPartyTypeConfiguration ()
        {
                
        }
    }
}
