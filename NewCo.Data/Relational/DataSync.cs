﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class DataSync
    {
        public DataSync()
        {

        }

        public DataSync(DataSyncName name, DataSyncStatus status)
        {
            this.SyncName = name;
            this.Status = status;
        }

        [Key]
        public int id { get; set; }

        [NotMapped]
        public DataSyncName SyncName { get; set; }
        [Column("SyncName")]
        [StringLength(32)]
        public string SyncNameString 
        {
            get { return this.SyncName.ToString(); }
            set { this.SyncName = (DataSyncName) Enum.Parse(typeof(DataSyncName), value); }
        }

        [NotMapped]
        public DataSyncStatus Status { get; set; }
        [Column("Status")]
        [StringLength(24)]
        public string StatusString
        {
            get { return this.Status.ToString(); }
            set { this.Status = (DataSyncStatus)Enum.Parse(typeof(DataSyncStatus), value); }
        }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime SyncTimeUTC { get; set; }

        [StringLength(768)]
        public string SyncHash { get; set; }
        
        public string Log { get; set; }

        [NotMapped]
        public DateTime LastSuccessfullSyncDateUTC { get; set;}
    }

    public enum DataSyncStatus { Running=1, Complete, Failed}
    public enum DataSyncName { Orders=1, Drivers}
}
