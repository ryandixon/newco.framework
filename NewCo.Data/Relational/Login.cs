﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Login : EntityHistoryBase
    {
        public Login()
        {
        }

        public Int64 Id { get; set; }
        /// <summary>
        /// The business entity associated with this login - Agent, Shipper, IPD User, etc.
        /// </summary>
        public Int64 BusinessEntityId { get; set; }
        [StringLength(32)]
        public string UserName { get; set; }
        public Guid ContactId { get; set; }

        [StringLength(64)]
        public string PasswordHash { get; set; }
        [StringLength(32)]
        public string PasswordSalt { get; set; }
        [StringLength(258)]
        public string PasswordQuestion { get; set; }
        [StringLength(32)]
        public string PasswordAnswer { get; set; }
        public bool IsLocked { get; set; }

        public int FailedPasswordAttemptCount { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastFailedPasswordAttemptDateUTC { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastSuccessfulLoginTime { get; set; }
    }
}
