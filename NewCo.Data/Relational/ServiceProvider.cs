﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NewCo.Data.Relational
{
    //public class ServiceProvider : ICreateHistory, IUpdateHistory
    public class ServiceProvider : EntityHistoryBase, IHasEncodedSequenceId
    {
        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private const string sequenceName = "seqServiceProviderId";
        string IHasEncodedSequenceId.SequenceName { get { return sequenceName; } }

        /// <summary>
        /// Driver number
        /// </summary>
        [StringLength(16)]
        [Index("idx_ServiceProviderCode")]
        public string Code { get; set; }

        //[Required]
        public long BusinessEntityId { get; set; }
        public virtual BusinessEntity BusinessEntity { get; set; }

        /// <summary>
        /// The mobile number associated with the driver's account
        /// </summary>
        [Required]
        [StringLength(12)]
        public string MobileNumber { get; set; }

        public Guid? CurrentVehicleProfile { get; set; }

        [NotMapped]
        public ServiceProviderEnrollmentStatus EnrollmentStatus { get; set; }

        [Required]
        [Column("Status")]
        [StringLength(16)]
        public string EnrollmentStatusString {
            get { return this.EnrollmentStatus.ToString(); }
            set { this.EnrollmentStatus = (ServiceProviderEnrollmentStatus)Enum.Parse(typeof(ServiceProviderEnrollmentStatus), value); }
        }

        /// <summary>
        /// A unique invitation code valid for a specific IC
        /// </summary>
        [StringLength(16)]
        public string InvitationCode { get; set; }

        public double? YearsExperience { get; set; }

        [StringLength(1023)]
        public string AboutMe { get; set; }

        public int MobileLoginId { get; set; }
        public virtual Login MobileLogin { get; set; }

        //NEED:
        //INSURANCE Docs and Specifics

        //[DateTimeKind(DateTimeKind.Utc)]
        //public DateTime CreateTimeUTC { get; set; }
        //public string CreatedBy { get; set; }

        //[DateTimeKind(DateTimeKind.Utc)]
        //public DateTime? LastUpdatedTimeUTC { get; set; }
        //public string LastUpdatedBy { get; set; }

    }

    public enum ServiceProviderEnrollmentStatus { New=1, Active, Suspended}
}
