﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class TransportPiece
    {
        public TransportPiece()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public decimal WeightLbs { get; set; }
        /// <summary>
        /// Length of the longest dimension in inches
        /// </summary>
        public decimal LongestDimensionInches { get; set; }
        /// <summary>
        /// The sum of the height width and depth in inches
        /// </summary>
        public decimal TotalDimensionInches { get; set; }

        [StringLength(40)]
        public string BarCode { get; set; }
    }
}
