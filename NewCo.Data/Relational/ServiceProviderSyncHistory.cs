﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewCo.Data.Relational
{
    public class ServiceProviderSyncHistory
    {
        [Key]
        public Int64 Id { get; set; }

        /// <summary>
        /// The BE ID of the service provider
        /// </summary>
        [Required]
        [ForeignKey("ServiceProvider")]
        public Int64 ServiceProviderId { get; set; }
        public virtual ServiceProvider ServiceProvider { get; set; }


        [NotMapped]
        public DataSyncName SyncName { get; set; }
        [Column("SyncName")]
        [StringLength(16)]
        public string SyncNameString
        {
            get { return this.SyncName.ToString(); }
            set { this.SyncName = (DataSyncName)Enum.Parse(typeof(DataSyncName), value); }
        }

        public DateTime? LastCompletedUTC { get; set; }

        public DateTime? LastAttemptedUTC { get; set; }


        [StringLength(8000)]
        public string ErrorInfo { get; set; }

    }

}
