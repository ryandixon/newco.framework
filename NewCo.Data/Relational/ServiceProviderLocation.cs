﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Represents a location update from a mobile device. 
    /// </summary>
    public class ServiceProviderLocation
    {
        [Key]
        public long ServiceProviderId { get; set; }

        [Key]
        public DateTime MeasurementTimeUTC { get; set; }


        [Index("idx_ServiceProviderLocation", Order=1)]
        public decimal Latitude { get; set; }
        [Index("idx_ServiceProviderLocation", Order=2)]
        public decimal Longitude { get; set; }

        public decimal Accuracy { get; set; }
    }
}
