﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    internal interface IHasEncodedSequenceId
    {
        long Id { get; set; }

        string Code { get; set; }

        string SequenceName { get; }
    }
}
