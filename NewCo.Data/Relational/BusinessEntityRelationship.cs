﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /*
     * Thinking ahead here but, suppose we have a wholesale customer (a courier) doing a delivery for one of their customers and that customer is also a retail customer of NewCo.
     * This would at least allow us to recognize that relationship and, perhaps, permit the customer to view orders that he's place with the agent along side orders he's placed directly
     * with NewCo.
     * 
     * I think there would need to be some kind of setup process here to establish the relationship but, not going to worry about that yet.
     * 
     * RDD 7/9/2015
     */
    /// <summary>
    /// Defines a business relationship between 2 entities.
    /// </summary>
    public class BusinessEntityRelationship
    {
        public BusinessEntityRelationship()
        {
            this.Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }

        public long BusinessEntityAId { get; set; }

        public long BusinessEntityBId { get; set; }

        public BusinessEntityRelationshipType Type { get; set; }

    }

    public enum BusinessEntityRelationshipType {AIsACustomerOfB, AIsAnAngentForB}
}
