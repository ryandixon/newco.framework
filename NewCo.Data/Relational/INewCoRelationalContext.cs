﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace NewCo.Data.Relational
{
    public interface INewCoRelationalContext
    {
        //
        // Summary:
        //     Creates a Database instance for this context that allows for creation/deletion/existence
        //     checks for the underlying database.
        System.Data.Entity.Database Database { get; }

        /// <summary>
        /// Rejects all pending changes in the context
        /// </summary>
        void RejectChanges();
        void SetEntryState(object entity, System.Data.Entity.EntityState state);

        System.Data.Entity.DbSet<Accessorial> Accessorials { get; set; }
        System.Data.Entity.DbSet<Address> Addresses { get; set; }
        System.Data.Entity.DbSet<Agent> Agents { get; set; }
        System.Data.Entity.DbSet<BusinessEntity> BusinessEntities { get; set; }
        System.Data.Entity.DbSet<BusinessEntityExternalIdentifier> BusinessEntityExternalIdentifiers { get; set; }
        System.Data.Entity.DbSet<BusinessEntityGroupMembership> BusinessEntityGroupMemberships { get; set; }
        System.Data.Entity.DbSet<BusinessEntityGroup> BusinessEntityGroups { get; set; }
        System.Data.Entity.DbSet<BusinessEntityRelationship> BusinessEntityRelationships { get; set; }
        System.Data.Entity.DbSet<BusinessEntitySettlementMethod> BusinessEntitySettlementMethods { get; set; }
        System.Data.Entity.DbSet<Contact> Contacts { get; set; }
        System.Data.Entity.DbSet<DataSync> DataSyncs { get; set; }
        System.Data.Entity.DbSet<ExternalOrderId> ExternalOrderIds { get; set; }
        System.Data.Entity.DbSet<Login> Logins { get; set; }
        System.Data.Entity.DbSet<OrderAccessorial> OrderAccessorials { get; set; }
        System.Data.Entity.DbSet<OrderProduct> OrderProducts { get; set; }
        System.Data.Entity.DbSet<Order> Orders { get; set; }
        System.Data.Entity.DbSet<OrderSecondaryParty> OrderSecondaryParties { get; set; }
        System.Data.Entity.DbSet<PerformanceRating> PerformanceRatings { get; set; }
        System.Data.Entity.DbSet<Product> Products { get; set; }
        System.Data.Entity.DbSet<Route> Routes { get; set; }
        int SaveChanges();
        System.Data.Entity.DbSet<ServiceProvider> ServiceProviders { get; set; }
        System.Data.Entity.DbSet<ServiceProviderSyncHistory> ServiceProviderSyncHistories { get; set; }
        System.Data.Entity.DbSet<Stop> Stops { get; set; }
        System.Data.Entity.DbSet<StopTransportPiece> StopTransportPieces { get; set; }
        System.Data.Entity.DbSet<TransportPiece> TransportPieces { get; set; }
    }
}
