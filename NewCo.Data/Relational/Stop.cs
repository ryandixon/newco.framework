﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Stop : EntityHistoryBase
    {
        public Stop()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        /// <summary>
        /// Id of order that this stop is included in. Allways null if RouteId is populated.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Id of predefined route. Always null if OrderId is populated.
        /// </summary>
        public Guid? RouteId { get; set; }


        public Guid? AddressId { get; set; }
        public Address Address { get; set; }
        /*
         * The address fields are duplicated here to capture an accurate snapshot of the address at the
         * the time the order was created. Subsequent changes to the address in the address table will not be reflected
         * in these fields
         */
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(96)]
        public string Street1 { get; set; }
        [StringLength(96)]
        public string Street2 { get; set; }
        [StringLength(40)]
        public string City { get; set; }
        [StringLength(40)]
        public string State { get; set; }
        [StringLength(24)]
        public string PostalCode { get; set; }
        [StringLength(64)]
        public string Country { get; set; }

        [Range(-179.9999999999, 179.9999999999)]
        public decimal? Latitude { get; set; }

        [Range(-179.9999999999, 179.9999999999)]
        public decimal? Longitude { get; set; }

        public int TimeZone { get; set; }

        
        /// <summary>
        /// The !LOCAL! "from" time that a service provider should not arrive prior to when completing a stop. Expressed as !LOCAL TIME! to the stop's location - NOT UTC!
        /// Using local time is important because the begin time is, for example, 8am even if a daylight savings time shift occurs between the stop/order creation and the actual event
        /// </summary>
        public DateTime? ScheduledBeginTime { get; set; }

        /// <summary>
        /// The !LOCAL! "to" time that a service provider should not arrive after to when completing a stop. Expressed as !LOCAL TIME! to the stop's location - NOT UTC!
        /// Using local time is important because the end time is, for example, 5pm even if a daylight savings time shift occurs between the stop/order creation and the actual event
        /// </summary>
        public DateTime? ScheduledEndTime { get; set; }
        

        /// <summary>
        /// When true, the service provider must go inside a building to complete the stop.
        /// </summary>
        public bool BuildingEntranceRequired { get; set; }

        [StringLength(80)]
        public string ContactName { get; set; }
        
        [StringLength(16)]
        public string ContactPhoneNumber { get; set; }
        [StringLength(80)]
        public string ContactEmail { get; set; }
        [StringLength(1043)]
        public string Note { get; set; }

        public bool SignatureRequired { get; set; }
        [StringLength(80)]
        public string NameOfSignor { get; set; }
        public Guid? SignatureImageId { get; set; }
        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? StopCompletionTime { get; set; }

        [StringLength(1048)]
        public string ServiceProviderNote { get; set; }

    }
}
