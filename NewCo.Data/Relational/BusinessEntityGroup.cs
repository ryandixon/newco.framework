﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// A group of entites as defined by another BE. Designed to allow shippers to put drivers into groups of their own making but, could conceivable be used by drivers to put order originators into groups as well
    /// </summary>
    public class BusinessEntityGroup :EntityHistoryBase
    {
        public BusinessEntityGroup()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        /// <summary>
        /// The Business Entity defining this group
        /// </summary>        
        public Int64 BusinessEntityId { get; set; }

        public BusinessEntityGroupType? Type { get; set; }

        /// <summary>
        /// Group type defined by the BusinessEntityd that created this group
        /// </summary>
        public string CustomGroupType { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

    }

    public enum BusinessEntityGroupType { Approved, Blocked, Custom}


}
