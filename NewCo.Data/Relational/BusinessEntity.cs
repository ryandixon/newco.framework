﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Any contractor or business that we might pay or receive money from.
    /// </summary>
    public class BusinessEntity : EntityHistoryBase, IHasEncodedSequenceId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        string IHasEncodedSequenceId.SequenceName { get { return "seqBusinessEntityId"; } }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]         //Need a trigger or the like to set the value of this column!!!
        [StringLength(16)]
        [Index("idx_BusinessEntityCode")]
        public string Code { get; set; }



        public int? ParentBusinessEntityId { get; set; }
        
        [Required]
        [StringLength(255)]
        public string OrganizationName { get; set; }

        public Guid? PrimaryContactId { get; set; }
        public virtual Contact PrimaryContact { get; set; }

        public Guid? BillingAddressId { get; set; }
        public virtual Address BillingAddress { get; set; }

        public Guid? PhysicalAddressId { get; set; }
        public virtual Address PhysicalAddress { get; set; }
        
        //The ID of the customer in the Wintao Code base
        public Guid? IPDAccountId { get; set; }

        public bool IsApprovedForPayment { get; set; }
        public bool IsApprovedForOrderEntry { get; set; }


        public Guid? PreferredSettlementMethodId { get; set; }
        public virtual BusinessEntitySettlementMethod PreferredSettlementMethod { get; set; }

    }


}
