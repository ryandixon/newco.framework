﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewCo.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NewCo.Data.Relational
{
    public class Order: EntityHistoryBase, IHasEncodedSequenceId
    {
        public Order()
            :base()
        {
            //Id = Guid.NewGuid();
            Status = OrderStatus.New;
            StatusTimeUTC = DateTime.UtcNow;
            CollectionStatus = OrderCollectionStatus.Unbilled;
            PaymentStatus = OrderPaymentStatus.Unpaid;
            SecondaryParties = new List<OrderSecondaryParty>();
            AutoAssignToFirstBidder = false;
            
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        string IHasEncodedSequenceId.SequenceName { get { return "seqOrderId"; } }

        /// <summary>
        /// A system assigned unique string identifying this order and is comprised of an encoded SourceDBInstanceID and RecordId
        /// </summary>
        //NOTE: this is SET via a trigger on insert of row!!!
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(16)]
        [Index("idx_OrderCode")]
        public string Code { get; set; }


        public static long OrderNumberAsId(string orderNumber)
        {
            return IDEncoder.DecodeIdToLong(orderNumber);
        }

        /// <summary>
        /// When not null, the guid of the route this order was created from.
        /// </summary>
        public Guid? RouteId { get; set; }

        /// <summary>
        /// The id of the BusinessEntity who originated this order. This is the entity from whom funds will be collected to for the completion of this order.
        /// </summary>
        [Required]
        [ForeignKey("Originator")]
        public Int64 OriginatorId { get; set; }
        
        public virtual BusinessEntity Originator { get; set; }

        /// <summary>
        /// The id of the BusinessEntity who is the service provider that will complete this order. This is the entity that will be paid.
        /// </summary>
        [ForeignKey("ServiceProvider")]
        public Int64? ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }

        [NotMapped]
        public OrderStatus Status { get; set; }

        [Required]
        [Column("Status")]
        [StringLength(16)]
        public string StatusString
        {
            get { return this.Status.ToString(); }
            set { this.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), value); }
        }

        [Required]
        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime StatusTimeUTC { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? CompletionTimeUTC { get; set; }

        [NotMapped]
        public OrderPaymentStatus PaymentStatus { get; set; }
        [Required]
        [Column("PaymentStatus")]
        [StringLength(16)]
        public string PaymentStatusString
        {
            get { return this.PaymentStatus.ToString(); }
            set { this.PaymentStatus = (OrderPaymentStatus)Enum.Parse(typeof(OrderPaymentStatus), value); }
        }

        [NotMapped]
        public OrderCollectionStatus CollectionStatus { get; set; }
        [Required]
        [Column("CollectionStatus")]
        [StringLength(16)]
        public string CollectionStatusString
        {
            get { return this.CollectionStatus.ToString(); }
            set { this.CollectionStatus = (OrderCollectionStatus)Enum.Parse(typeof(OrderCollectionStatus), value); }
        }


        /// <summary>
        /// The stated driver settlement amount. I.e. the amount agreed to by the order orginator and service provider for completion of the order
        /// </summary>
        public decimal? ServiceProviderOfferAmount { get; set; }
        
        public int? OfferExpirationSeconds { get; set; }
        public decimal? DeclaredValue { get; set; }
        public bool? IncludesDangerousGoods { get; set; }

        public virtual List<OrderSecondaryParty> SecondaryParties { get; set; }

        /// <summary>
        /// When true, this order should automatically be assigned to the first service provider to bid on it.
        /// </summary>
        public bool AutoAssignToFirstBidder { get; set; }

    }

    public enum OrderStatus { New = 10, Offered = 20, Assigned = 30, Started = 40, Complete = 50, Canceled = 60, Aborted = 70, Closed = 80 }

    public enum OrderPaymentStatus { Unpaid = 10, PaymentPending = 20, PartiallyPaid = 30, Complete = 40 }

    public enum OrderCollectionStatus { Unbilled = 10, Billed = 20, PartiallyCollected = 30, Complete = 40 }

    public class OrderTypeConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderTypeConfiguration()
        {
            HasMany(o => o.SecondaryParties)
                .WithOptional()
                .WillCascadeOnDelete(false);
        }
    }

}
