﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Infrastructure;
using NewCo.Data;
namespace NewCo.Data.Relational
{
    public class NewCoRelationalContext: ContextBase, INewCoRelationalContext
    {
         public NewCoRelationalContext(string nameOrConnectionString, System.Security.Principal.IIdentity userIdentity)
            : base(nameOrConnectionString, userIdentity)
        {
            
            
        }
         public NewCoRelationalContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            
        }

        public NewCoRelationalContext( System.Security.Principal.IIdentity userIdentity)
            : base("NewCoRelationalContext", userIdentity)
         {

         }


        public enum NewCoSequence { BusinessEntityId = 1, ServiceProviderId, OrderId }

        private long GetNextSequenceValue(string sequenceName)
        {

            return this.Database.SqlQuery<Int64>(String.Format("SELECT NEXT VALUE FOR dbo.{0}", sequenceName)).Single();
        }

        internal void SetNextSequenceValue(IHasEncodedSequenceId entity)
        {
            if (entity.Id < 1)//Only if they have not already assigned an ID here (which they would have had to explicitly do already).
                //Fetch Sequence
                entity.Id = this.GetNextSequenceValue(entity.SequenceName);

            if (String.IsNullOrEmpty(entity.Code) || IDEncoder.DecodeIdToLong(entity.Code)!=entity.Id)
                entity.Code = IDEncoder.GenerateRandomizedIdHash(entity.Id);

        }

        internal void PopulateSequenceIds()
        {
            foreach (DbEntityEntry<IHasEncodedSequenceId> entry in ChangeTracker.Entries<IHasEncodedSequenceId>().Where(e => e.State == EntityState.Added))
            {
                SetNextSequenceValue(entry.Entity);
            }
        }
        public override int SaveChanges()
        {
            PopulateSequenceIds();
            return base.SaveChanges();
        }

        public DbSet<Accessorial> Accessorials { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<BusinessEntity> BusinessEntities { get; set; }
        public DbSet<BusinessEntityExternalIdentifier> BusinessEntityExternalIdentifiers { get; set; }
        public DbSet<BusinessEntityGroup> BusinessEntityGroups { get; set; }
        public DbSet<BusinessEntityGroupMembership> BusinessEntityGroupMemberships { get; set; }
        public DbSet<BusinessEntityRelationship> BusinessEntityRelationships { get; set; }
        public DbSet<BusinessEntitySettlementMethod> BusinessEntitySettlementMethods { get; set; }
        public DbSet<Char32EncodingAlphabet> Char32EncodingAlphabet { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<DataSync> DataSyncs { get; set; }
        public DbSet<ExternalOrderId> ExternalOrderIds { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderAccessorial> OrderAccessorials { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        public DbSet<OrderSecondaryParty> OrderSecondaryParties { get; set; }
        public DbSet<PerformanceRating> PerformanceRatings { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceProviderSyncHistory> ServiceProviderSyncHistories { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopTransportPiece> StopTransportPieces { get; set; }
        public DbSet<TransportPiece> TransportPieces { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {

            var providerName = this.GetProviderName(this.Database.Connection.ConnectionString);
            switch (providerName)
            {
                case "Ngpsql":
                    builder.HasDefaultSchema("public");
                    break;
                case "System.Data.SqlClient":
                    builder.Entity<Char32EncodingAlphabet>()
                        .Property(p => p.Character)
                        .HasColumnType("char");
                break;
            }

            builder.Conventions.Remove<PluralizingTableNameConvention>();

            builder.Configurations.Add(new OrderTypeConfiguration());

        }


    }
}
