﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Address : EntityHistoryBase
    {
        public Address ()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(96)]
        public string Street1 { get; set; }
        [StringLength(96)]
        public string Street2 { get; set; }
        [StringLength(40)]
        public string City { get; set; }
        [StringLength(40)]
        public string State { get; set; }
        [StringLength(24)]
        public string PostalCode { get; set; }
        [StringLength(64)]
        public string Country { get; set; }

        [Range(-179.9999999999, 179.9999999999)]
        public decimal? Latitude { get; set; }

        [Range(-179.9999999999, 179.9999999999)]
        public decimal? Longitude { get; set; }

        public int TimeZone { get; set; }

    }
}
