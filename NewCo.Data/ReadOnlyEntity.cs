﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public abstract class ReplicateEntity
    {
        [Key]
        public int ReplicateId { get; set; }

        public int ShardId { get; set; }

        public int Id { get; set; }


        [StringLength(32)]
        ///An char32 encoded value in the for of <ShardId>-<Obfuscated EntityId>
        public string IdCode { get; set; }
    }
}
