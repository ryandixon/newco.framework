﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewCo.Models;
using NewCo.Models.Api;
namespace NewCo.Service
{
    public interface IOrderService
    {
        IEnumerable<Order> GetOrders(int skip = 0, int take = 50);
    }
}
