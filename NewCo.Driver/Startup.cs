﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewCo.Driver.Startup))]
namespace NewCo.Driver
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
