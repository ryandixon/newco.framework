﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Models.Api
{
    public class GpsCoordinates
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
