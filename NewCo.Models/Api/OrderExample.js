﻿/*
    OrderExample.js - JSON and explanation of the Order JavaScript object to be
    sent to the Order REST API developed by Datatrac.  Information in this file
    is considered to be preliminary and may change as the system is developed.

    Order - object
    {
        OrderCode - string, length 16 characters, Optional.  
            The string code assigned by the Datatrac API when an Order has been
			successfully created.  Shall be blank or empty on new Orders.  Shall
			be passed on Order update.
		ShippingCustomer - ShippingCustomer object, Optional.
			The ShippingCustomer object that represents the information about the customer
			that is shipping the piece.  See below for the definition of the ShippingCustomer
			object.
		ShippingCustomerOrderId - string, length 32 characters, Optional.
			The string reference number of the shipping customer when the shipment
			is planned.
        OriginatorCustomerId - string, length 32 characters, Optional.
            The string ID of the shipping customer as represented in the originator
			system.
		OriginatorCustomerTypeId - string, length 32 characters, Optional.
			The string Type ID of the shipping customer as represented in the 
			originator system.
		OriginatorOrderId - string, length 32 characters, Optional.
			The string reference number of the order as represented in the originator
			system.
		ServiceProviderCode - string, length 16 characters, Optional.
			The string code assigned by the Datatrac API when a Service Provider has
			been created.  Shall be passed on new Orders and on Order update.
		CompletionDateTimeUTC - number, Optional.
			The Date and Time at which the Order must be completed.  Shall be passed as 
			UNIX epoch time (milliseconds since 01/01/1970 00:00:00.000 UTC).
		GrossSettlementAmount - number, Optional.
			The numeric value of the amount to be paid to the Service Provider upon the
			successful completion of the Order.  Value defaults to United States Dollar.
		DeclaredValue - number, Optional.
			The numeric value of the Order.  Value defaults to United States Dollar.
		MaxLength - number, Optional.
			The numeric value of the maximum length of the piece to be shipped.  Value 
			defaults to inches.
		TotalWeight - number, Optional.
			The numeric value of the total weight of all pieces in the shipment.  Value
			defaults to pounds.
		IncludesDangerousGoods - boolean, Optional.
			The boolean value that determines if the shipment contains any goods that are
			considered dangerous.  Shall be passed as JavaScript true or false.
		Accessorials - Array of strings, each string length 16 characters, Optional.
			The Array of strings that indicate what accessories will be required in order to
			complete the Order successfully.  Valid values are 'LiftGate', 'HandTruck', and
			'FurnitureDolly'.
		Stops - Array of Stop objects, Optional.
			The Array of Stop objects that defines the Route that the Service Provider is to
			take to complete the Order.  Shall be passed with at least one set of Stops 
			representing one pickup and one delivery.  See below for the definition of the Stop
			object.
    }
	
	ShippingCustomer - object
	{
		NOTE: ShippingCustomer Persistence
			ShippingCustomers will be attempted to be persisted by the Datatrac API for reuse.
			The OrganizationName and ContactPhoneNumber will be used to look up the ShippingCustomer.
			If the ShippingCustomer is not found, it will be created.  If the ShippingCustomerCode is
			passed, the ShippingCustomer details will be updated.  

		ShippingCustomerCode - string, length 16 characters, Optional.
			The string code assigned by the Datatrac API when a Shipping Customer
			has been created.  See Note above on ShippingCustomer persistence.
		OrganizationName - string, length 75 characters, Optional.
			The name of the customer shipping the piece.
		ContactFirstName - string, length 40, Optional.
			The first name of the customer.
		ContactLastName - string, length 40, Optional.
			The last name of the customer.
		ContactPhoneNumber - string, length 24 characters, Optional.
			The phone number of the customer.
		ContactEmail - string, length 95, Optional.
			The email of the customer.
	}

	Stop - object
	{
		NOTE:  Address Persistence
			Addresses will be persisted by the Datatrac API for reuse.  If the GUID Address.Id is not passed, 
			the Address details (Street1, Street2, City, State, PostalCode, and Country) will be used to look up 
			the Address.  If the Address is not found in the Datatrac API, the Address details will be validated by 
			a geocoding service and a new Address will be created for reuse in a future Order.  At least one value
			(Address.Id or a complete set of Address details) must be passed for the Order to be created.  If both 
			values are passed, the GUID Id takes precedence, with the Address details being updated.  If neither is 
			passed, the Order will fail validation.

        Address - Address object, Required.
			The Address object that stores information about the Stop.  See Note about 
			Address persistence.  See below for the definition of the Address object.
		SequenceNumber - number, Required.
			The numeric value indicating the sequence of the Stop within the Array of Stops.  The value
			shall be a 1-based ordinal.
		Name - string, length 255 characters, Optional.
			The string name of the Stop - assigned by the originator and stored by the Datatrac API.
        ContactName - string, length 75 characters, Optional.
			The string name of the person to receive the shipment at the Stop.  
        ContactPhoneNumber - string, length 24 characters, Optional.
			The string phone number of the person to receive the shipment at the Stop.
        BuildingEntranceRequired - boolean, Optional.
			The boolean value that determines if the shipment will require entrance into the Stop
			location.  Shall be passed as JavaScript true or false.
        SignatureRequired - boolean, Optional.
			The boolean value that determines if delivery of the shipment requires a signature.  
			Shall be passed as JavaScript true or false.
		ArrivalFromUTC - number, Optional.
			The Date and Time after which the Service Provider must arrive at the Stop.  Shall be passed
			as UNIX epoch time (milliseconds since 01/01/1970 00:00:00.000 UTC).
		ArrivalToUTC - number, Optional.
			The Date and Time before which the Service Provider must arrive at the Stop.  Shall be passed
			as UNIX epoch time (milliseconds since 01/01/1970 00:00:00.000 UTC).
        NumberOfItems - number, Optional.
			The numeric value of the quantity of pieces to be picked up at this Stop.
	}
			
	Address - object
	{
		NOTE: Latitude and Longitude
			The Latitude and Longitude are used only in the event that the Address details cannot be
			validated by a geocoding service.  
			
        Street1 - string, length 96 characters, Required.
			The string Street information of the Address.
        Street2 - string, length 96 characters, Optional.
			The string continued Street information of the Address.
        City - string, length 40 characters, Required.
			The string City information of the Address.
        State - string, length 40 characters, Required.
			The string State information of the Address.
        PostalCode - string, length 24 characters, Required.
			The string Postal Code information of the Address.
        Country - string, length 64 characters, Optional.
			The string Country information of the Address.
		Latitude - number, range -179.9999999 to +179.9999999, Optional.
			The numeric value of the Latitude of the Address with up to seven digits of precision.
			See Note about Latitude use.
		Longitude - number, range -179.9999999 to +179.9999999, Optional.
			The numeric value of the Longitude of the Address with up to seven digits of precision.
			See Note about Longitude use.
		BaseTimeZoneOffsetFromUTC - number, range -14 hours (-840 minutes) to +14 hours (+840 minutes), Required.
			The numeric value of the time zone offset (in minutes) from UTC for Standard Time in 
			which the Address is located.  
	}
*/

/*
	Example JSON for new Order - newOrder.MaxLength and newOrder.TotalWeight have not been specified,
	implying that they are not provided as part of the Order.  newOrder.ShippingCustomer identifies a 
	new ShippingCustomer will be created.  newOrder.Stops[0] identifies a new Address will be created.
	newOrder.Stops[1].Id has been specified, implying that the Address object already created will be 
	used.
*/
var newOrder = {
	ShippingCustomer: {
		OrganizationName: "Company Name",
		ContactFirstName: "FirstName",
		ContactLastName: "LastName",
		ContactPhoneNumber: "970-555-9874",
		ContactEmail: "noreply@contoso.com"
	},
	ShippingCustomerOrderId: "478965413",
	OriginatorCustomerId: "QAZ12345",
	OriginatorOrderId: "589632147",
	ServiceProviderCode: "UYV321",
	CompletionDateTime: 1440013732067,
	GrossSettlementAmount: 50.00,
	DeclaredValue: 100.00,
	IncludesDangerousGoods: false,
	Accessorials: [ "HandTruck" ],
	Stops: [ {
			Name: "Stop 1",
			Address: {
				Street1: "12345 Street Name",
				Street2: "Apt. 567",
				City: "Fort Collins",
				State: "CO",
				PostalCode: "80526",
				Country: "USA",
				Latitude: 40.552334,
				Longitude: -105.129021,
				BaseTimeZoneOffsetFromUTC: -420
			},
			SequenceNumber: 1,
			ContactName: "Shipper Contact Name",
			ContactPhoneNumber: "970-555-1234 Ext. 543",
			BuildingEntranceRequired: false,
			SignatureRequired: true,
			ArrivalFromUTC: 1440015663514,
			ArrivalToUTC: 1440015687017,
			NumberOfItems: 1
		}, {
			Name: "Stop 2",
			Address: {
				Id: "80d8a88f-5d64-4a97-8044-8a3882838f4f",
			},
			SequenceNumber: 2,
			ContactName: "Receiver Contact Name",
			ContactPhoneNumber: "970-555-5678",
			BuildingEntranceRequired: true,
			SignatureRequired: true,
			ArrivalFromUTC: 1440015663514,
			ArrivalToUTC: 1440015687017,
			NumberOfItems: 1
		}
	]
}
