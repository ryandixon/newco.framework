﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Models.Api
{
    public class Stop
    {
        public Guid Id { get; set; }

        public Address Address { get; set; }

        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }

        public string Name { get; set; }

        public bool? BuildingEntranceRequired { get; set; }
        public bool? SignatureRequired { get; set; }

        public DateTime ArrivalFromUTC { get; set; }
        public DateTime ArrivalToUTC { get; set; }

        public int NumberOfItems { get; set; }

    }
}
