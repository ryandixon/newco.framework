﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace NewCo.Models.Api
{
    public class Order: IOrder
    {
        public string OrderCode { get; set; }

        /*
         * Since a customer is likely a reusable resource, we'll require clients of the API to first create a customer BE and reference that entity here.
         */
        /// <summary>
        /// NewCo's identifier for the BusinessEntity associated with the customer
        /// </summary>
        public string ShippingCustomerCode { get; set; }

        public string ShippingCustomerOrderId { get; set; }

        /// <summary>
        /// The Agent's identifier for the BusinessEntity associated with the customer
        /// </summary>
        public string OriginatorCustomerId { get; set; }
        public string OriginatorCustomerIdType { get; set; }
        public string OriginatorOrderId { get; set; }


        /// <summary>
        /// The name of the customer
        /// </summary>
        public string ShippingCustomerName { get; set; }

        /// <summary>
        /// The Code of the Service Provider (Driver) that will complete the order
        /// </summary>
        public string ServiceProviderCode { get; set; }

        public decimal? GrossSettlementAmount { get; set; }

        public DateTime CompletionDateTime { get; set; }


        public IEnumerable<Stop> Stops { get; set; }

        public decimal? DeclaredValue { get; set; }
        public bool? IncludesDangerousGoods { get; set; }

        public int MaxLength { get; set; }
        public int TotalWeight { get; set; }

        public string[] Accessorials { get; set; }
    }
    public interface IOrder { }
}
