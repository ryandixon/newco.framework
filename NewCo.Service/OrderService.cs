﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewCo.Data;
using AutoMapper;
using Api = NewCo.Models.Api;
using Relational = NewCo.Data.Relational;


namespace NewCo.Service
{
    public class OrderService: RelationalServiceBase, IOrderService
    {
        public OrderService(NewCo.Data.Relational.INewCoRelationalContext relationalContext, NewCoServiceUser userIdentity,  Lazy<IBusinessEntityService> businessEntityService)
            : base(relationalContext, userIdentity)
        {
            _businessEntityService = businessEntityService;
        }
        protected readonly Lazy<IBusinessEntityService> _businessEntityService;
        protected IBusinessEntityService businessEntityService { get { return _businessEntityService.Value;}}

        public IEnumerable<Api.Order> GetOrders(int skip=0, int take=50)
        {
            return Mapper.Map<IEnumerable<Relational.Order>, IEnumerable<Api.Order>>(
                relationalContext.Orders.Where(o => o.OriginatorId == serviceUser.BusinessEntityId)
                .OrderByDescending(o => o.CreateDateUTC)
                .Skip(skip)
                .Take(take)
                );
        }

        /// <summary>
        /// Insert a new order into the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Api.Order Add(Api.Order model)
        {
            var order = new Relational.Order();
            order.OriginatorId = serviceUser.BusinessEntityId;

            AssignShipper(order, model);

            if (!string.IsNullOrEmpty(model.ServiceProviderMobileNumber))
                order.ServiceProviderId = (from sp in relationalContext.ServiceProviders
                                        where sp.MobileNumber == model.ServiceProviderMobileNumber
                                        select sp.Id)
                                        .First();



            return Mapper.Map<Relational.Order,Models.Api.Order>(order);
        }

        /// <summary>
        /// Searches for an matching existing record for the shipper, if not found it creates a new one and associates it the the provided order
        /// </summary>
        /// <param name="order"></param>
        /// <param name="model"></param>
        internal void AssignShipper(Relational.Order order, Api.Order model)
        {
            var secondaryParty = new Relational.OrderSecondaryParty
            {
                Relationship = Relational.SecondaryPartyRelationship.Shipper
            };

            var shipper = FindCustomer(model).Select(be => be.Id).FirstOrDefault();
            if (shipper == null)
            {
                var be = new Relational.BusinessEntity();
                be.OrganizationName = model.CustomerName;


                var beRelationship = new Relational.BusinessEntityRelationship
                {
                    BusinessEntityAId = order.OriginatorId,
                    Type = Relational.BusinessEntityRelationshipType.AIsACustomerOfB
                };
                relationalContext.BusinessEntityRelationships.Add(beRelationship);
            }
            order.SecondaryParties.Add(secondaryParty);
        }

        internal IQueryable<Relational.BusinessEntity> FindCustomer(Api.Order model)
        {
            IQueryable<Relational.BusinessEntity> rtn = null;
            if (rtn == null && !String.IsNullOrEmpty(model.OriginatorCustomerId))
            {
                rtn = from be in relationalContext.BusinessEntities
                      join beeId in relationalContext.BusinessEntityExternalIdentifiers on be.Id equals beeId.BusinessEntityIdSource
                      where
                          beeId.Identifier == model.OriginatorCustomerId
                          && beeId.IdentifierType == model.OriginatorCustomerIdType
                      select be;
            }

            if (model.CustomerId > 0)
            {
                //verify that they have an established relationship with the existing BE
                rtn = from be in relationalContext.BusinessEntities
                            join ber in relationalContext.BusinessEntityRelationships on be.Id equals ber.BusinessEntityBId
                       where
                         be.Id == model.CustomerId
                         && ber.Type == Relational.BusinessEntityRelationshipType.AIsACustomerOfB
                       select be;
            }

            if (rtn == null && !String.IsNullOrEmpty(model.CustomerName))
            {
                //Look for a customer that they have an established relationship with that has the saem name
                rtn = from be in relationalContext.BusinessEntities
                      join ber in relationalContext.BusinessEntityRelationships on be.Id equals ber.BusinessEntityBId
                      where
                        be.Id == model.CustomerId
                        && ber.Type == Relational.BusinessEntityRelationshipType.AIsACustomerOfB
                      select be;
            }


            return rtn;
        }

        
    }
}
