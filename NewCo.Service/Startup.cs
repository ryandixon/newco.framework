﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Relational = NewCo.Data.Relational;
using Model = NewCo.Models.Api;

namespace NewCo.Service
{
    public class Startup
    {
        public static void ConfigureMapping()
        {
            Mapper.CreateMap<Relational.OrderStatus, string>()
                .ConvertUsing(src => src.ToString());

            Mapper.CreateMap<Relational.Order, Model.Order>()
                .ForMember(t => t.ServiceProviderMobileNumber, o => o.MapFrom(src => src.ServiceProvider.MobileNumber))
            //    .ForMember(t => t.CustomerNumber, o => o.MapFrom(src => src.OrderSecondaryParties.Where(osp => osp.Relationship == Relational.SecondaryPartyRelationship.Shipper);
            ;

            Mapper.CreateMap<Model.Order, Relational.Order>()
                
            ;
            
        }
    }
}
