﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Service
{
    public class RelationalServiceBase
    {
        protected readonly NewCo.Data.Relational.INewCoRelationalContext relationalContext;
        protected readonly NewCoServiceUser serviceUser;

        public RelationalServiceBase(NewCo.Data.Relational.INewCoRelationalContext relationalContext, NewCoServiceUser userIdentity)
        {
            this.relationalContext = relationalContext;
            this.serviceUser = userIdentity;
        }

    }
}
