﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Service
{
    public class NewCoServiceUser: IIdentity
    {
        private readonly IIdentity userIdentity;
        public NewCoServiceUser(IIdentity userIdentity, int businessEntityId)
        {
            this.userIdentity = userIdentity;
            this.beId = businessEntityId;
        }

        private readonly int beId;
        public int BusinessEntityId { get { return beId; } }

        public string AuthenticationType
        {
            get { return userIdentity.AuthenticationType; }
        }

        public bool IsAuthenticated
        {
            get { return userIdentity.IsAuthenticated; }
        }

        public string Name
        {
            get { return userIdentity.Name; }
        }
    }
}
