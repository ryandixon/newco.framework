﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;

namespace NewCo.WebAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }

        public ApplicationUser(string userName)
            : base(userName)
        {

        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            userIdentity.AddClaim(new Claim("businessEntityId", this.BusinessEntityId.ToString()));                        
            // Add custom user claims here
            return userIdentity;
        }

        public int BusinessEntityId { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            //: base("DefaultConnection", throwIfV1Schema: false)
            : base("ApiDbContext", throwIfV1Schema: false)            
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class ApplicationDbContextInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var apiRole = context.Roles.Add(new IdentityRole("ApiUser"));
            try
            {
                var apiUser = new Models.ApplicationUser("devUser")
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "rdixon@datatrac.com",
                    BusinessEntityId = 40000,
                    SecurityStamp="thisInitialValueDoesNotMatter",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = false,
                };

                apiUser.Roles.Add(new IdentityUserRole { 
                    UserId = apiUser.Id,
                    RoleId = apiRole.Id 
                });
                apiUser.Claims.Clear();
                apiUser.Claims.Add(new IdentityUserClaim
                {
                    UserId = apiUser.Id,
                    ClaimType = "hasRegistered",
                    ClaimValue = "true"
                });

                apiUser.PasswordHash = new PasswordHasher().HashPassword("Datatrac1");
                context.Users.Add(apiUser);
                context.SaveChanges();

                var serviceProviderRole = context.Roles.Add(new IdentityRole("ServiceProvider"));

                var serviceProviderUser = new Models.ApplicationUser("6782502500")
                {
                    Id  = Guid.NewGuid().ToString(),
                    Email = "rdixon+NewCo.Driver.User@datatrac.com",
                    BusinessEntityId = 40000,
                    SecurityStamp = "thisInitialValueDoesNotMatterEither",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = false,
                };

                serviceProviderUser.Roles.Add(new IdentityUserRole
                {
                    UserId = serviceProviderUser.Id,
                    RoleId = serviceProviderRole.Id
                });
                serviceProviderUser.Claims.Clear();
                serviceProviderUser.Claims.Add(new IdentityUserClaim
                {
                    UserId = serviceProviderUser.Id,
                    ClaimType = "hasRegistered",
                    ClaimValue = "true"
                });

                serviceProviderUser.PasswordHash = new PasswordHasher().HashPassword("Datatrac1");
                context.Users.Add(serviceProviderUser);
                context.SaveChanges();

            }
            catch (Exception e)
            { 
                throw;
            }
            base.Seed(context);
        }
    }
}