﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

using Autofac;
using NewCo.WebAPI.Filters;
using System.Reflection;
using Autofac.Integration.WebApi;

namespace NewCo.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //var builder = new ContainerBuilder();

            ////builder.RegisterAssemblyTypes(
            ////    Assembly.GetExecutingAssembly())
            ////    .Where(t => !t.IsAbstract && typeof(ApiController).IsAssignableFrom(t))
            ////    .InstancePerMatchingLifetimeScope("MyHTTPRequest");
            ////    //.InstancePerMatchingLifetimeScope(AutofacWebApiDependencyResolver);
            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //builder.RegisterModule()

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Filters.Add(new IdentityBasicAuthenticationAttribute());

#if !DEBUG
            config.Filters.Add(new NewCo.WebAPI.Filters.RequireHttpsAttribute());
#endif

            //Make JSON the default format for browser calls (which uses "text/html")
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
