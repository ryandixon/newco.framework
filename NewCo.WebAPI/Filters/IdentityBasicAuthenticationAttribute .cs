﻿﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NewCo.WebAPI.Models;

//using BasicAuthentication.Data;

namespace NewCo.WebAPI.Filters
{
    public class IdentityBasicAuthenticationAttribute : BasicAuthenticationAttribute
    {
        protected override async Task<IPrincipal> AuthenticateAsync(string userName, string password, CancellationToken cancellationToken)
        {
            //UserManager<IdentityUser> userManager2; //CreateUserManager();
            UserManager<ApplicationUser> userManager = CreateUserManager();

            cancellationToken.ThrowIfCancellationRequested(); // Unfortunately, UserManager doesn't support CancellationTokens.
            //IdentityUser user = await userManager.FindAsync(userName, password);
            var user = await userManager.FindAsync(userName, password);

            if (user == null)
            {
                // No user with userName/password exists.
                return null;
            }

            // Create a ClaimsIdentity with all the claims for this user.
            cancellationToken.ThrowIfCancellationRequested(); // Unfortunately, IClaimsIdenityFactory doesn't support CancellationTokens.
            ClaimsIdentity identity = await userManager.ClaimsIdentityFactory.CreateAsync(userManager, user, "Basic");
            //ClaimsIdentity identity2 = await userManager2.ClaimsIdentityFactory.CreateAsync(userManager2, user, "Basic");
            return new ClaimsPrincipal(identity);
        }

        //private static UserManager<IdentityUser> CreateUserManager()
        //{
        //    return new UserManager<IdentityUser>(new UserStore<IdentityUser>(new NewCo.WebAPI.Models.ApplicationDbContext()));
        //}

        private static UserManager<ApplicationUser> CreateUserManager()
        {
            var rtn =  new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new NewCo.WebAPI.Models.ApplicationDbContext()));
            //rtn.ClaimsIdentityFactory = new AppUserClaimsIdentityFactory();
            return rtn;
        }
    }

    public class AppUserClaimsIdentityFactory : ClaimsIdentityFactory<ApplicationUser, string>
    {
        public async override Task<ClaimsIdentity> CreateAsync(UserManager<ApplicationUser, string> manager,
                ApplicationUser user, string authenticationType)
        {
            var identity = await base.CreateAsync(manager, user, authenticationType);
            //identity.AddClaim(new Claim(ClaimTypes.Country, user.Country));

            return identity;
        }
    }
}