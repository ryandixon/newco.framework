﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NewCo.Service;
using NewCo.Models.Api;
using NewCo.WebAPI.Filters;
namespace NewCo.WebAPI.Controllers
{
    [EarlyAuthorizationAttribute]
    [Authorize]
    public class OrderController : ApiController
    {
        private Lazy<IOrderService> orderService;
        public OrderController(Lazy<IOrderService> orderService)
        {
            this.orderService = orderService;
        }
        //public OrderController()
        //{
            
        //}

        //private IOrderService _orderService;
        //private IOrderService orderService
        //{
        //    get
        //    {
        //        if (_orderService == null)
        //        {
        //            _orderService = new OrderService(new NewCo.Data.Relational.NewCoRelationalContext(User.Identity), new NewCoServiceUser(User.Identity, 40000));
        //        }
        //        return _orderService;
        //    }
        //}

        /// <summary>
        /// Retrieves a specific order based on the order number
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns>Order </returns>
        [Route("api/order/{orderNumber}")]
        [HttpGet]        
        public IHttpActionResult Get(string orderNumber)
        {
            //AgentOrder order = new AgentOrder()
            //{
            //    Id = Guid.NewGuid(),
            //    OrderNumber = "00123456",                 
            //};
            //order.Stops.Add(new Stop()
            //{
            //    Id = Guid.NewGuid(),
            //    ContactNumber = "30396055",
            //    ContactName = "Fred"
            //});

            return Ok();
        }

        [Route("api/order")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            //GlobalConfiguration.Configuration.DependencyResolver.res
            return Ok(orderService.Value.GetOrders());
        }

        public IHttpActionResult PostOrder(Order order)
        {

            return Ok();
        }
    }
}
