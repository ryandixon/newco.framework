﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Owin.Security;

using Autofac;
using Autofac.Integration.WebApi;

using NLog;

using NewCo.Data.Relational;
using NewCo.Data.Sequences;
using NewCo.WebAPI.Models;
using System.Reflection;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NewCo.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static IContainer Container { get; set; }

        protected void Application_Start()
        {
            NewCo.Service.Startup.ConfigureMapping();

            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            builder.RegisterType<ApplicationDbContext>().As<DbContext>().InstancePerLifetimeScope();

            builder.Register((c) =>
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    //NOTE that this actually produces a 500 error because we are still trying to build the controller itself (usually) when this executes. Not sure how to fix this.
                    //throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
                    return null;

                ApplicationUserManager _userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = _userManager.FindByName<ApplicationUser, string>(HttpContext.Current.User.Identity.Name);
                return new NewCo.Service.NewCoServiceUser(HttpContext.Current.User.Identity, user.BusinessEntityId);
            })
            .InstancePerRequest();

            builder.Register((c) =>
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    //throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
                    return null;

                return new NewCoRelationalContext(HttpContext.Current.User.Identity);
            })
            .As<INewCoRelationalContext>()
            .InstancePerRequest();

            //builder.Register((c) =>
            //{
                
            //    //var logManager = new NLog.LogManager();
            //    //return new NLog.LogManager.GetCultureInfo;
            //})
            //.As<NLog.ILogger>()
            //.InstancePerLifetimeScope();

            builder.RegisterType<NewCo.Service.OrderService>()
                .As<NewCo.Service.IOrderService>()
                .InstancePerRequest();

            builder.RegisterType<NewCo.Service.BusinessEntityService>()
                .As<NewCo.Service.IBusinessEntityService>()
                .InstancePerRequest();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            Database.SetInitializer(new ApplicationDbContextInitializer());
            using (var appContext = new ApplicationDbContext())
            {
                appContext.Database.Initialize(false);
            }
            

            Database.SetInitializer(new NewCo.Data.Relational.NewCoRelationalInitializer());
            using (NewCoRelationalContext db = new NewCoRelationalContext(new System.Security.Principal.GenericIdentity("DbInitializer")))
            {
                db.Database.Initialize(false);
            }

            //Database.SetInitializer(new NewCo.Data.Sequences.SequenceContextInitializer());
            //using (SequenceContext db = new SequenceContext())
            //{
            //    db.Database.Initialize(false);
            //}

        }
    }
}
